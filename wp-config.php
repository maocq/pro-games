<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'dbprogames');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'X+]=cFLI~}Ykvby$Oetd4)P1]P;.$++YfX$jQV+Y|<(4%Ci7H&R+_>VX?&+Jx#r7');
define('SECURE_AUTH_KEY', 'Xk|:C/:{$)@*cH(Em1[yQ1wK{h(my=[]||Dx^nVU4lc!!$X7#@7ZScpV3g2Ga4!q');
define('LOGGED_IN_KEY', 'kQKawRxR!xUs,C&g+BRnJ_D4FxsmfVV&M&A^OFsP7`:ibU]NWbNJ|T[wyHO.2&#Q');
define('NONCE_KEY', 'U$7$w?yv0A;#xev:(|M;2rsm%R-[.a%q95aKMMGH;hCIYfTk!yGvX+;GXe)eD|,:');
define('AUTH_SALT', '%Hl{SWjuz9ahQ&O~HZ`VNa-t9g, M81t4I ;rJ{?6m[q:[]4@W-?E-.{m%X~M9yy');
define('SECURE_AUTH_SALT', ';#kt0ra0+5nMS[XzQd)dD~Nlrv:i3[B|%6b-;*;T<^coo2dRxzf2b2T! n);A6>`');
define('LOGGED_IN_SALT', 'iGvs&-C;bQ4+y-c_W#I/Ev{T FH6-rud;n){&b X~n{| MShCXmc{(D!8@h1>7[h');
define('NONCE_SALT', 'h+A00KBC@1slh+thZn7vAN.aJr#84HuV?g<dW.k|}f68|3@|o<kE*[~*&|4VCp{K');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'prog_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

