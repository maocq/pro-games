<?php
/*
 Template Name: Página Full
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

    <?php if ($imgID = get_post_thumbnail_id($post->ID)): ?>

        <section id="bannerEstatico" class="">

            <figure>
                <img src="<?= wp_get_attachment_image_src( $imgID, 'full' )['0']; ?>" alt="">
                <figcaption>
                    <h1><?= get_the_title(); ?></h1>
                </figcaption>
            </figure>

        </section>

    <?php endif ?>

    <section lass="container">
        <article class="cita">
            <div class="text-left entry-content">
                <?php if (!$imgID): ?>
                    <h1><?= get_the_title(); ?></h1>
                <?php endif ?>
                <span class="lineaCuadros"></span>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile; endif;?>
            </div>
            <div class="lineaCuadros"></div>
        </article>
    </section>

<?php get_footer(); ?>
