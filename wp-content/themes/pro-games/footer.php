			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

                <div class="container">
                    <article class="col-md-3 col-sm-6 col-xs-12">
                        <h3>MENÚ</h3>
                        <?php wp_nav_menu(array(
                                 'container' => false,
                                 'menu' => __( 'The Social Menu', 'pro-games' ),
                                 'menu_class' => '',
                                 'theme_location' => 'footer-links',
                        )); ?>
                    </article>
                    <article class="col-md-3 col-sm-6 col-xs-12 contacto">
                        <?php if (is_active_sidebar('menu-contacto')): ?>
                            <?php dynamic_sidebar('menu-contacto'); ?>
                        <?php endif ?>
                    </article>
                    <figure class="col-md-3 col-sm-6 col-xs-12">
                        <a href="<?= site_url() ?>"><img src="<?= get_site_icon_url() ?>" alt="<?php bloginfo('name'); ?>"></a>
                    </figure>
                    <article id="siguenos" class="col-md-3 col-sm-6 col-xs-12">
                        <h3>SÍGUENOS</h3>
                        <?php wp_nav_menu(array(
                                 'container' => false,
                                 'menu' => __( 'The Social Menu', 'pro-games' ),
                                 'menu_class' => 'social',
                                 'theme_location' => 'social-links',
                        )); ?>
                    </article>

                </div>
                <article id="derechos">
                    <div class="container">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <p><strong><?= date('Y'); ?> © Pro - Games</strong> - Todos los derechos reservados</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <p>Web Design - <strong>Dafernam </strong></p>
                        </div>
                    </div>
                </article>

			</footer>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
