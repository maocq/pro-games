<?php get_header(); ?>

    <?php if ($imgID = get_post_thumbnail_id($post->ID)): ?>

        <section id="bannerEstatico" class="">

            <figure>
                <img src="<?= wp_get_attachment_image_src( $imgID, 'full' )['0']; ?>" alt="">
                <figcaption>
                    <h1 class="wow fadeInLeft"><?= get_the_title(); ?></h1>
                </figcaption>
            </figure>

        </section>

    <?php endif ?>

    <section class="textoRegular container">
        <article>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; endif;?>
        </article>
    </section>

    <section id="contactoForm" class="container">
        <div class="contenedorMed">

            <?= do_shortcode('[contact-form-7 id="62" title="Contacto"]'); ?>

        </div>

    </section>


    <section id="ubicacion">
        <?= get_field('map_contact') ?>
    </section>

<?php get_footer(); ?>
