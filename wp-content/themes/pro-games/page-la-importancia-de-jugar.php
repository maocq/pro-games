<?php get_header(); ?>

    <?php if ($imgID = get_post_thumbnail_id($post->ID)): ?>

        <section id="bannerEstatico" class="">

            <figure>
                <img src="<?= wp_get_attachment_image_src( $imgID, 'full' )['0']; ?>" alt="">
                <figcaption>
                    <h1 class="wow fadeInLeft"><?= get_the_title(); ?></h1>
                </figcaption>
            </figure>

        </section>

    <?php endif ?>

    <section class="container">
        <article class="cita">
            <div>
                <span class="lineaCuadros"></span>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile; endif;?>
            </div>
            <div class="lineaCuadros"></div>
        </article>
    </section>
    <?php if (have_rows('benefits')): ?>
        <section class="beneficios contenedorPq">
            <article>
                <figure><img src="<?= get_template_directory_uri(); ?>/library/images/titulo-beneficios.png" alt="Estos son algunos beneficios que su empresa puede tener al jugar."></figure>
                    <?php while ( have_rows('benefits') ) : the_row();?>
                        <ul class="wow fadeIn" data-wow-offset="150">
                            <li class="col-md-2 col-sm-2 col-xs-12">
                                <h1><?= get_sub_field('number'); ?></h1>
                            </li>
                            <li class="col-md-8 col-sm-8 col-xs-12"><?= get_sub_field('benefits'); ?></li>
                            <li class="col-md-2 col-sm-2 col-xs-12"><span class="icon-<?= get_sub_field('icon'); ?>"></span></li>
                        </ul>
                    <?php endwhile; ?>
            </article>
        </section>
    <?php endif ?>

    <section class="container contactoHome">

        <article class="contactoInterior">
            <?php if (have_rows('benefits')): ?>
                <h2> Y mucho más...</h2>
            <?php endif ?>
            <span class="lineaCuadros"></span>
            <?= get_field('footer_importancia_jugar'); ?>
        </article>
    </section>

<?php get_footer(); ?>
