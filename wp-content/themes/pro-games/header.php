<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>

	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

			<header class="header" role="banner" itemscope itemtype="http://schema.org/WPHeader">

                <div class="container">
                    <div id="logoMobile">
                        <figure>
                            <a href="<?= site_url() ?>"><img src="<?= get_site_icon_url() ?>" alt="<?php bloginfo('name'); ?>"></a>
                        </figure>
                    </div>
                    <a href="#" class="menuMobile"><span></span></a>
                    <nav>

                        <?php if (is_active_sidebar('menu-principal-sidebar')): ?>
                            <?php dynamic_sidebar('menu-principal-sidebar'); ?>
                        <?php endif ?>

                        <?php wp_nav_menu(array(
                                 'container' => false,
                                 'menu' => __( 'The Social Menu', 'pro-games' ),
                                 'menu_class' => 'social',
                                 'theme_location' => 'social-links',
                        )); ?>

                    </nav>
                </div>

			</header>
