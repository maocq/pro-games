<?php get_header(); ?>

    <?php if (have_rows('slider_home')): ?>
        <section id="sliderHome" class="swiper-container">

            <div class="swiper-wrapper">
                <?php while ( have_rows('slider_home') ) : the_row();?>
                    <figure class="swiper-slide container">
                        <img src="<?= get_sub_field('image_slider')['url']; ?>" alt="">
                        <figcaption class="container">
                            <h1><?= get_sub_field('title_slider'); ?></h1>
                            <a href="<?= get_sub_field('link_slider'); ?>"><?= get_sub_field('phrase_link'); ?> <span class="icon-flecha"></span></a>
                        </figcaption>
                    </figure>
                <?php endwhile; ?>
            </div>

            <div class="pagination container"></div>
        </section>
    <?php endif ?>

    <section class="container">
        <article class="iconoTexto">
            <div>
                <span class="icon-control wow fadeInDown" data-wow-delay="1s"></span>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile; endif;?>
            </div>
        </article>
    </section>

    <?php if (have_rows('articles_home')): $count = 1;?>

        <section class="container menuHome">

            <?php while ( have_rows('articles_home') ) : the_row();?>
                <article class="imagen-menu wow fadeInUp" data-wow-offset="100">
                    <figure class="imagen-color col-md-6 col-sm-6 col-xs-12">
                        <div class="<?= getClass($count) ?>"></div>
                        <figcaption>
                            <div>
                                <h2><?= get_sub_field('title_article'); ?></h2>
                                <p><?= get_sub_field('message_article'); ?></p>
                                <a href="<?= get_sub_field('link_article'); ?>"></a>
                            </div>
                        </figcaption>
                    </figure>
                    <figure class="imagen-home col-md-4 col-sm-4 col-xs-12">
                        <img src="<?= get_sub_field('image_article')['url']; ?>" alt="">
                    </figure>
                    <div class="col-md-2 col-sm-2"></div>
                </article>
                <?php ++$count; ?>
            <?php endwhile; ?>

        </section>
    <?php endif ?>
    <section class="container contactoHome">

        <article class="contactoInterior">
            <span class="lineaCuadros"></span>
            <?= get_field('footer_home'); ?>
        </article>
    </section>

<?php get_footer(); ?>
