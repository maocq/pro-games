    <?php get_header(); ?>

    <?php if ($imgID = get_post_thumbnail_id($post->ID)): ?>

        <section id="bannerEstatico" class="">

            <figure>
                <img src="<?= wp_get_attachment_image_src( $imgID, 'full' )['0']; ?>" alt="">
                <figcaption>
                    <h1 class="wow fadeInLeft"><?= get_the_title(); ?></h1>
                </figcaption>
            </figure>

        </section>

    <?php endif ?>

    <section id="quienesSomos" class="contenedorPq">
        <article>
            <span class="lineaCuadros"></span>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; endif;?>
        </article>
    </section>

    <?php if (have_rows('team_progames')): ?>
        <section id="equipo" class="container">
            <h2 class="tituloCentral wow pulse" data-wow-offset="100" data-wow-delay="1s">Conoce el equipo</h2>
            <?php while ( have_rows('team_progames') ) : the_row();?>
                <figure class="col-md-4 col-sm-4 col-xs-12">
                    <img src="<?= get_sub_field('photo')['url']; ?>" alt="">
                    <figcaption>
                        <h2><?= get_sub_field('name'); ?></h2>
                        <p><?= get_sub_field('position'); ?></p>
                    </figcaption>
                </figure>
            <?php endwhile; ?>
        </section>
    <?php endif ?>

<?php get_footer(); ?>
