<?php get_header(); ?>

    <?php if ($imgID = get_post_thumbnail_id($post->ID)): ?>

        <section id="bannerEstatico" class="">

            <figure>
                <img src="<?= wp_get_attachment_image_src( $imgID, 'full' )['0']; ?>" alt="">
                <figcaption>
                    <h1 class="wow fadeInLeft"><?= get_the_title(); ?></h1>
                </figcaption>
            </figure>

        </section>

    <?php endif ?>

    <section class="container">
        <article class="cita">
            <div>
                <span class="lineaCuadros"></span>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile; endif;?>
            </div>
            <div class="lineaCuadros"></div>
        </article>
    </section>

    <?php if (have_rows('games')): ?>
        <section id="queHacemos" class="container">
            <div class="contenedorMed">
                <div class="lineaAbajo"></div>
                <?php while ( have_rows('games') ) : the_row();?>
                    <article>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h2><?= get_sub_field('title_game'); ?></h2>
                            <p><?= get_sub_field('description_game'); ?>
                                <h1 class="wow pulse"><?= get_sub_field('number_game'); ?></h1>
                        </div>
                        <figure class="col-md-6 col-sm-6 col-xs-12">
                            <img src="<?= get_sub_field('image_game')['url']; ?>" alt="">
                        </figure>
                    </article>
                <?php endwhile; ?>
            </div>
        </section>
    <?php endif ?>

    <section class="container contactoHome">

        <article class="contactoInterior">
            <?php if (have_rows('games')): ?>
                <h2 class="wow bounceInLeft"> Y mucho más...</h2>
            <?php endif ?>
            <span class="lineaCuadros"></span>
            <?= get_field('footer_que_hacemos'); ?>
        </article>
    </section>

<?php get_footer(); ?>
