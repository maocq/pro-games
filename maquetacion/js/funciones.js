$(document).ready(function () {


    var mySwiper = new Swiper('.swiper-container', {
        pagination: '.pagination',
        paginationClickable: true,
        slidesPerView: 1,
        autoplay: 3000,
        keyboardControl: false,
        loop: true,

    });



    $('.imagen-color').each(function () {
        $(this).parent().find('.imagen-home img').clone().appendTo($(this));
    });

    $('.menuMobile').click(function () {

        $(this).toggleClass('menuAnimacion')
        $('header nav').slideToggle();


    })


});